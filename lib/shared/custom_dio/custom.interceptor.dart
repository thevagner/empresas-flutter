import 'package:dio/dio.dart';
import 'package:empresas/app/app_controller.dart';
import 'package:empresas/app/app_module.dart';
import 'package:empresas/shared/local_base/investor.helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CustomInterceptor extends InterceptorsWrapper {
  final AppController _appController = AppModule.to.get<AppController>();
  final UserHelper userHelper = AppModule.to.get<UserHelper>();

  @override
  onRequest(RequestOptions options) async {
    if (_appController.investorLogged != null) {
      Map<String, dynamic> header = {
        "Content-Type": "application/json",
        "access-token": _appController.investorLogged.token,
        "client": _appController.investorLogged.client,
        "uid": _appController.investorLogged.uid
      };
       options.headers = header;
    }
    return options;
  }

  @override
  onError(DioError err) async {
    debugPrint("======================================================");
    debugPrint("==============ERROR===${err.message} => ${err.request.path}============");
    debugPrint("======================================================");
    print(Modular.to.path);
    if (Modular.to.path!="/login"&&(err.response.statusCode == 401 || err.response.statusCode == 400)) {
      await Modular.to.showDialog(
          builder: (_) => AlertDialog(
                title: Text("Faça login"),
                content: Text(
                  "Para continuar você precisa estar logado.",
                  style: TextStyle(color: Colors.grey, fontSize: 14),
                ),
                actions: [TextButton(onPressed: () => Modular.to.pop(), child: Text("OK"))],
              ));
      await userHelper.deleteUser();
      _appController.setUserLogged(null);
      Modular.to.pushReplacementNamed('/login');
    }
    return err;
  }

  @override
  onResponse(Response response) async {
    debugPrint("REQUEST[${response.statusCode}] => ${response.request.path}${response.request.queryParameters}");

    //200 ou 201

    return response;
  }
}
