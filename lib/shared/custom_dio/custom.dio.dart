import 'package:dio/dio.dart';
import 'package:empresas/shared/custom_dio/custom.interceptor.dart';
import 'package:empresas/utils/constants.dart';

class CustomDio{

  Dio client;

  CustomDio(this.client){
    client.options.baseUrl = BASE_URL;
    client.interceptors.add(CustomInterceptor());
  }

}