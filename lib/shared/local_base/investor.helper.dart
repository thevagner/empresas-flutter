import 'package:empresas/app/modules/login/models/investor.model.dart';
import 'package:empresas/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class UserHelper {

  static final _investorTable = "investor"; //TABELA
  static final _idColumn = "id";
  static final _investorNameColumn = "investor_name";
  static final _emailColumn = "email";
  static final _cityColumn = "city";
  static final _countryColumn = "country";
  static final _balanceColumn = "balance";
  static final _photoColumn = "photo";
  static final _portifolioValueColumn = "portfolio_value";
  static final _first_accessColumn = "first_access";
  static final _superAngelColumn = "super_angel";
  static final _tokenColumn = "token";
  static final _clientColumn = "client";
  static final _uuidColumn = "uid";

  static Database _database;
  static UserHelper _userHelper;

  UserHelper._createInstance();

  factory UserHelper(){
    if (_userHelper == null) {
      // executado somente uma vez
      debugPrint("[INSTANCE FROM DATA BASE AS LOADING...]");
      _userHelper = UserHelper._createInstance();
    }
    return _userHelper;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await _userHelper.initializeDatabase();
    }
    return _database;
  }

  Future<Database> initializeDatabase() async {
    // Get a location using getDatabasesPath
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, DB_USER);

    _database = await openDatabase(path, version: DB_VERSION, onCreate: _createDb);
    debugPrint("[INSTANCE FROM DATA BASE AS LOADED => OK]");

    return _database;
  }

  _createDb(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        'CREATE TABLE $_investorTable '
            '('' $_idColumn INTEGER PRIMARY KEY'',  '' $_investorNameColumn TEXT,         '' $_emailColumn TEXT'''
            ','' $_cityColumn TEXT'',            '' $_countryColumn TEXT'',            '' $_balanceColumn REAL'''
            ','' $_photoColumn TEXT'',           '' $_portifolioValueColumn REAL'',    '' $_first_accessColumn TEXT'''
            ','' $_superAngelColumn TEXT'',           '' $_tokenColumn TEXT'',              '' $_clientColumn TEXT'''
            ','' $_uuidColumn TEXT'')');

    debugPrint("[TABLE OF USERS CREATED]");
  }

//==============================  USER ================================================

  Future<Investor> insertInvestor(Investor investor) async {
    Database _db = await this.database;
    try{
      debugPrint("[DB LOCAL][INSERT] INVESTOR ${investor.investorName} NO BANCO DE DADOS...");
      await _db.insert(_investorTable, investor.toJson());
    } on DatabaseException catch (e) {
      if (e.isUniqueConstraintError()) {
        await updateInvestor(investor);
      }
    }
    debugPrint("[DB LOCAL][ADD] ${investor.investorName}");
    return investor;
  }

  Future<Investor> getUser() async {
    debugPrint("[DB LOCAL][GET] USUARIOS");

    Database _db = await this.database;
    List<Map> maps = await _db.query(_investorTable,
        columns: [
          _idColumn,
          _investorNameColumn,
          _emailColumn,
          _cityColumn,
          _countryColumn,
          _balanceColumn,
          _photoColumn,
          _portifolioValueColumn,
          _first_accessColumn,
          _superAngelColumn,
          _tokenColumn,
          _clientColumn,
          _uuidColumn
        ]);
    debugPrint('[GET RESULT DABATABSE] QUANTIDADE DE USUARIOS ENCONTRADOS ${maps.length}');
    if (maps.length > 0) {
      return Investor.fromJson(maps.first);
    }
    return null;
  }

  updateInvestor(Investor investor) async {
    Database _db = await this.database;
    await _db.update(_investorTable, investor.toJson(),
        where: '$_idColumn = ?', whereArgs: [investor.id]);
    debugPrint("[DB LOCAL][UPDATE] ATUALIZANDO USUÁRIO ${investor.investorName} NO BANCO DE DADOS...");
  }

  deleteUser()async{
    Database _db = await this.database;
    await _db.delete(_investorTable);
  }
}