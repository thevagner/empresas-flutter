
class Validators{

  static validaEmail(str){
    if(str.isEmpty){
      return "Digite seu e-mail";
    }else if(!RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(str)){
      return "Email inválido";
    }
    return null;
  }


  static validateEmpty(str){
    if(str.isEmpty){
      return "Campo obrigatório";
    }
    return null;
  }
}