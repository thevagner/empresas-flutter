
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

/////  LOCAL CONSTANTS
final String API_VERSION  = "v1";
final String BASE_URL = "https://empresas.ioasys.com.br/api/$API_VERSION";

// ENDPOINTS CONSTANTS
final String AUTH_POINT = "/users/auth/sign_in";
final String ENTERPRISES_POINT = "/enterprises";

////// DB CONSTANTS
final String DB_USER = "DB_USER_V6";
final int DB_VERSION = 6;


// MOCK
List<String> sortedImages = [
  "https://images.unsplash.com/photo-1504384308090-c894fdcc538d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
  "https://images.unsplash.com/photo-1604459278088-84160a62fe90?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=744&q=80",
  "https://images.unsplash.com/photo-1591049433264-618fa2f4558f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=968&q=80",
  "https://images.unsplash.com/photo-1564090373-a73d13e552c5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=675&q=80",
  "https://images.unsplash.com/photo-1560067879-0bc7a88e6cfb?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1189&q=80",
  "https://images.unsplash.com/photo-1568155048480-8b7130779bba?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1189&q=80",
  "https://images.unsplash.com/photo-1612144572426-73ac61a7b876?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1000&q=80",
  "https://images.unsplash.com/photo-1591053442076-495738f08c12?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=968&q=80",
  "https://images.unsplash.com/photo-1591049290662-bd7574ab05d6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1050&q=80"
];

Map<String, dynamic> icons = {
  "Todos":LineAwesomeIcons.suitcase,
  "Marketplace":LineAwesomeIcons.shopping_cart,
  "HR Tech":LineAwesomeIcons.circle_o_notch,
  "Service":LineAwesomeIcons.cog,
  "Social":LineAwesomeIcons.share_square,
  "Fintech":LineAwesomeIcons.suitcase,
  "Transport":LineAwesomeIcons.car,
  "Health":LineAwesomeIcons.heart,
  "Music":LineAwesomeIcons.music,
  "Green":FontAwesomeIcons.seedling,
  "IoT":FontAwesomeIcons.robot,
  "Software":LineAwesomeIcons.bug,
  "Industry":LineAwesomeIcons.industry,
  "Food":FontAwesomeIcons.pizzaSlice,
  "IT":LineAwesomeIcons.laptop,
};
