import 'package:dio/dio.dart';
import 'package:empresas/app/modules/login/login_module.dart';
import 'package:empresas/app/modules/splash/splash_module.dart';
import 'package:empresas/shared/custom_dio/custom.dio.dart';
import 'package:empresas/shared/local_base/investor.helper.dart';

import 'app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:empresas/app/app_widget.dart';
import 'package:empresas/app/modules/home/home_module.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i)=>AppController()),

        Bind((i)=>CustomDio(i.get<Dio>())),
        Bind((i)=>Dio()),
        Bind((i)=>UserHelper()),

  ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, module: SplashModule()),
        ModularRouter('/login', module: LoginModule()),
        ModularRouter('/home', module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
