import 'package:empresas/app/modules/login/models/investor.model.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'app_controller.g.dart';

@Injectable()
class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {

  Investor investorLogged;

  setUserLogged(Investor investor)=>investorLogged=investor;

}
