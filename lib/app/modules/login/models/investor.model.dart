class Investor {
  int id;
  String investorName;
  String email;
  String city;
  String country;
  double balance;
  String photo;
  double portfolioValue;
  bool firstAccess;
  bool superAngel;
  String token;
  String uid;
  String client;

  Investor(
      {this.id,
        this.investorName,
        this.email,
        this.city,
        this.country,
        this.balance,
        this.photo,
        this.portfolioValue,
        this.firstAccess,
        this.superAngel,
        this.token,
        this.uid,
        this.client});

  Investor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    investorName = json['investor_name'];
    email = json['email'];
    city = json['city'];
    country = json['country'];
    balance = json['balance'];
    photo = json['photo'];
    portfolioValue = json['portfolio_value'];
    firstAccess = json['first_access']=="1"?true:false;
    superAngel = json['super_angel']=="1"?true:false;
    token = json['token'];
    uid = json['uid'];
    client = json['client'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['investor_name'] = this.investorName;
    data['email'] = this.email;
    data['city'] = this.city;
    data['country'] = this.country;
    data['balance'] = this.balance;
    data['photo'] = this.photo;
    data['portfolio_value'] = this.portfolioValue;
    data['first_access'] = this.firstAccess?"1":"0";
    data['super_angel'] = this.superAngel?"1":"0";
    data['token'] = this.token;
    data['uid'] = this.uid;
    data['client'] = this.client;
    return data;
  }
}
