import 'package:empresas/app/app_module.dart';
import 'package:empresas/app/modules/login/respository/auth_repository.dart';
import 'package:empresas/shared/custom_dio/custom.dio.dart';
import 'package:empresas/shared/local_base/investor.helper.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'login_controller.dart';
import 'login_page.dart';

class LoginModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i)=>LoginController()),
        Bind((i)=>AuthRepository(i.get<CustomDio>(), i.get<UserHelper>())),

        Bind((i)=>AppModule.to.get<UserHelper>()),
        Bind((i)=>AppModule.to.get<CustomDio>())
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => LoginPage()),
      ];

  static Inject get to => Inject<LoginModule>.of();
}
