import 'package:empresas/app/app_controller.dart';
import 'package:empresas/app/app_module.dart';
import 'package:empresas/app/modules/login/login_module.dart';
import 'package:empresas/app/modules/login/models/investor.model.dart';
import 'package:empresas/app/modules/login/respository/auth_repository.dart';
import 'package:empresas/shared/general_models/error_server.model.dart';
import 'package:empresas/utils/validator_util.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'login_controller.g.dart';

@Injectable()
class LoginController = _LoginControllerBase with _$LoginController;

abstract class _LoginControllerBase with Store {

  final PageController pageController = PageController();
  final AuthRepository _authRepository = LoginModule.to.get<AuthRepository>();
  final AppController _appController = AppModule.to.get<AppController>();

  @observable
  int page = 0;

  @observable
  bool obscure = false;

  @observable
  String error = "";

  @observable
  bool loading = false;

  String get textButton =>page==1?"Entrar":"Prosseguir";

  @action
  setPage(int value)=>page = value;

  @action
  setObscure()=>obscure=!obscure;

  jumpToEmail(){
    setErrorValue("");
    setPage(0);
    pageController.animateToPage(0, duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  jumpToPass()=>pageController.animateToPage(1, duration: Duration(milliseconds: 500), curve: Curves.decelerate);

  previousPage()=>pageController.previousPage(duration: Duration(milliseconds: 500), curve: Curves.decelerate);

  pop(){
    if(page==0){
      return true;
    }else{
      previousPage();
      return false;
    }
  }

  @action
  setErrorValue(String value)=>error=value;

  validateEmail(String str){
    setErrorValue("");
    String err = Validators.validaEmail(str);
    if(err!=null){
      setErrorValue(err);
    }
  }

  validatePass(String str){
    setErrorValue("");
    String err = Validators.validateEmpty(str);
    if(err!=null){
      setErrorValue(err);
    }
  }

  Future<dynamic> entrar(String email, String pass)async{
    dynamic result = await _authRepository.auth(email: email, pass: pass);
    if(result.runtimeType==Investor){
      Investor investor = result;
      _appController.setUserLogged(investor);
      Modular.to.pop();
      Modular.to.pushReplacementNamed('/home');
    }else if(result.runtimeType==ErrorServer){
      ErrorServer errorServer = result;
      setErrorValue(errorServer.errors[0]);
      Modular.to.pop();
    }else{
      error = "Ocorreu algum problema";
    }
  }
}
