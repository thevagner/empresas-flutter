import 'package:dio/dio.dart';
import 'package:empresas/app/modules/login/models/investor.model.dart';
import 'package:empresas/shared/custom_dio/custom.dio.dart';
import 'package:empresas/shared/general_models/error_server.model.dart';
import 'package:empresas/shared/local_base/investor.helper.dart';
import 'package:empresas/utils/constants.dart';
import 'package:flutter/material.dart';

class AuthRepository{

  CustomDio _customDio;
  UserHelper userHelper;
  AuthRepository(this._customDio, this.userHelper);

  dynamic auth({@required String email, @required pass})async{
    debugPrint("[DB SERVIDOR] ENVIANDO DADOS ${email} PARA O SERVIDOR");

    try {
      var response = await _customDio.client.post(AUTH_POINT, data: {"email":email, "password":pass});
      Investor investor = Investor.fromJson(response.data['investor']);
      investor.token = response.headers['access-token'].first;
      investor.client = response.headers['client'].first;
      investor.uid = response.headers['uid'].first;
      // await userHelper.deleteUser();
      await userHelper.insertInvestor(investor);
      return investor;
    } on DioError catch (e) {
      print(e.response);
      debugPrint("PROBLEMA AO OBTER DADOS DO SERVIDOR, DADOS CARREGADOS DO BANCO LOCAL");
      if (e.response == null) {
          return ErrorServer(success: false, errors: ["Você esta offline"]);
      } else if (e.response.statusCode != 200) {
        return ErrorServer.fromJson(e.response.data);
      }
    } catch (e){
      return ErrorServer(success: false, errors: ["Tivemos um problema"]);
    }
  }

}