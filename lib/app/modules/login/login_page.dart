import 'dart:ui';

import 'package:empresas/app/app_module.dart';
import 'package:empresas/app/modules/login/widgets/app_bar._top.widget.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

import 'login_controller.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  //use 'controller' variable to access controller

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passController = TextEditingController();

  @override
  void dispose() {
    controller.pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopAppBar(
        height: 120,
      ),
      body: WillPopScope(
        onWillPop: () async => controller.pop(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                allowImplicitScrolling: true,
                onPageChanged: (page) {
                  unFocus();
                  controller.setPage(page);
                },
                controller: controller.pageController,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 15),
                        child: TextFormField(
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(color: Colors.grey, fontSize: 18),
                          controller: _emailController,
                          decoration: InputDecoration(
                              hintText: "Digite aqui o seu e-mail",
                              alignLabelWithHint: true,
                              hintStyle: TextStyle(fontSize: 18),
                              isDense: false,
                              border: InputBorder.none,
                              errorBorder: InputBorder.none,
                              errorStyle: TextStyle(fontSize: 14)),
                        ),
                      ),
                      Observer(
                          builder: (_) => controller.error.isEmpty
                              ? Container()
                              : Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      LineAwesomeIcons.exclamation_triangle,
                                      color: Colors.red,
                                      size: 18,
                                    ),
                                    Text(
                                      controller.error,
                                      style: TextStyle(color: Colors.red),
                                    ),
                                  ],
                                ))
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Observer(
                        builder: (_) => Container(
                          padding: EdgeInsets.symmetric(horizontal: 30),
                          child: TextFormField(
                            textAlign: TextAlign.center,
                            obscureText: !controller.obscure,
                            keyboardType: TextInputType.text,
                            style: TextStyle(color: Colors.grey, fontSize: 18),
                            controller: _passController,
                            decoration: InputDecoration(
                              hintText: "Senha",
                              alignLabelWithHint: true,
                              hintStyle: TextStyle(fontSize: 18),
                              border: InputBorder.none,
                              suffixIcon: GestureDetector(
                                  onTap: () => controller.setObscure(),
                                  child: controller.obscure
                                      ? Icon(LineAwesomeIcons.eye_slash)
                                      : Icon(LineAwesomeIcons.eye)),
                            ),
                          ),
                        ),
                      ),
                      Observer(
                          builder: (_) => controller.error.isEmpty
                              ? Container()
                              : Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      LineAwesomeIcons.times_circle,
                                      color: Colors.red,
                                    ),
                                    Text(
                                      controller.error,
                                      style: TextStyle(color: Colors.red),
                                    ),
                                  ],
                                ))
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Observer(
                    builder: (_) => controller.page == 0
                        ? Container()
                        : TextButton(
                            onPressed: () => controller.jumpToEmail(),
                            child: Text("Alterar e-mail"),
                          ),
                  ),
                  Observer(
                    builder: (_) => controller.page == 1
                        ? Container()
                        : TextButton(
                      onPressed: (){},
                      child: Text("Criar conta"),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Observer(
                        builder: (_) => Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: RaisedButton(
                            padding: EdgeInsets.all(20),
                            onPressed: () => validar(),
                            elevation: 0,
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  controller.textButton,
                                  style: TextStyle(color: Colors.white),
                                ),
                                Icon(
                                  LineAwesomeIcons.arrow_right,
                                  color: Colors.white,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  unFocus(){
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  validar() {
    if (controller.page == 0) {
      validarEmail();
    } else if (controller.page == 1) {
      validarPassEntrar();
    }
  }

  validarEmail() {
    controller.validateEmail(_emailController.text);
    if (controller.error.isEmpty) {
        controller.jumpToPass();
    }
  }

  validarPassEntrar() async {
    controller.validatePass(_passController.text);
    if (controller.error.isEmpty) {
      showDialog(
          barrierDismissible: false,
          barrierColor: Colors.white70,
          context: context,
          builder: (context) => WillPopScope(
            onWillPop: ()async=>false,
            child: Dialog(
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  child: Container(
                        width: 80,
                        height: 80,
                        child: FlareActor("assets/anims/load_anim.flr",
                            animation: "loading", color: Theme.of(context).accentColor),
                      )),
          ));
      await controller.entrar(_emailController.text, _passController.text);
    }
  }
}
