import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopAppBar extends PreferredSize {

  double height;

  TopAppBar({@required this.height});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 30,
              child: Image.asset("assets/images/logo_.png", color: Color.fromRGBO(224, 30, 105, 1), fit: BoxFit.scaleDown,),
            ),
            Text("Seja bem vindo ao empresas!", style: TextStyle(color: Color.fromRGBO(224, 30, 105, 1), fontWeight: FontWeight.bold),)

          ],
        );
  }
}