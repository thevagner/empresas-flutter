import 'package:empresas/app/app_controller.dart';
import 'package:empresas/app/app_module.dart';
import 'package:empresas/shared/local_base/investor.helper.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'splash_controller.g.dart';

@Injectable()
class SplashController = _SplashControllerBase with _$SplashController;

abstract class _SplashControllerBase with Store {

  final UserHelper userHelper = AppModule.to.get<UserHelper>();
  final AppController _appController = AppModule.to.get<AppController>();

  initialize()async{
    _appController.setUserLogged(await userHelper.getUser());
    if(_appController.investorLogged==null){
      Modular.to.pushReplacementNamed('/login');
    }else{
      Modular.to.pushReplacementNamed('/home');
    }
  }

}
