import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';
import 'splash_controller.dart';

class SplashPage extends StatefulWidget {

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends ModularState<SplashPage, SplashController> {
  //use 'controller' variable to access controller
  MultiTrackTween tween;

  @override
  void initState() {
    tween = MultiTrackTween([
      Track("color1").add(Duration(seconds: 1),
          ColorTween(begin: Colors.purpleAccent, end: Colors.red)),
      Track("color2").add(Duration(seconds: 1),
          ColorTween(begin: Colors.purple, end: Colors.blue))
    ]);
    open();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          ImageFiltered(
            imageFilter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_home.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              // child: Image.asset("assets/images/logo_home.png"),
            ),
          ),
          ImageFiltered(
            imageFilter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: ControlledAnimation(
              playback: Playback.MIRROR,
              tween: tween,
              duration: tween.duration,
            builder: (context, animation) => Opacity(
              opacity: 0.4,
              child: Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [animation["color1"], animation["color2"]])),
                  ),
            ),
                // child: Image.asset("assets/images/logo_home.png"),
              ),
            ),
          Center(child: Image.asset("assets/images/logo_home.png", width: 150, height: 45,))
        ],
      ),
    );
  }

  open()async{
    await Future.delayed(Duration(seconds: 3));
    controller.initialize();
  }
}
