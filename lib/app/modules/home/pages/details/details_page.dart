
import 'package:cached_network_image/cached_network_image.dart';
import 'package:empresas/app/modules/home/models/enterprise.model.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

class DetailsPage extends StatefulWidget {
  Enterprise enterprise;
  DetailsPage(this.enterprise);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  //use 'controller' variable to access controller

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Theme.of(context).accentColor),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(widget.enterprise.enterpriseName, style: TextStyle(color: Colors.black, fontSize: 14),),
            Text(widget.enterprise.enterpriseType.enterpriseTypeName, style: TextStyle(color: Theme.of(context).accentColor, fontSize: 14),),
          ],
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: 150,
              child: CachedNetworkImage(
                imageUrl: widget.enterprise.photo,
                fit: BoxFit.cover,
                height: 200,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Text(
                    widget.enterprise.enterpriseName,
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 20, color: Colors.grey),
                  ),
                  Text(
                    "${widget.enterprise.city} - ${widget.enterprise.country}a",
                    style: TextStyle(
                        fontWeight: FontWeight.w300, fontSize: 14, color: Colors.grey),
                  ),
                  SizedBox(height: 20,),
                  Text(widget.enterprise.description, style: TextStyle(color: Colors.grey),),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(icon: Icon(LineAwesomeIcons.facebook, color: Colors.blue, size: 30,), onPressed: (){}),
                IconButton(icon: Icon(LineAwesomeIcons.twitter, color: Colors.blue, size: 30,), onPressed: (){}),
                IconButton(icon: Icon(LineAwesomeIcons.linkedin, color: Colors.blueAccent, size: 30,), onPressed: (){}),
              ],
            )
          ],
        ),
      ),
    );
  }
}
