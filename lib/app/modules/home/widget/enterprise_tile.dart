import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:empresas/app/modules/home/models/enterprise.model.dart';
import 'package:empresas/app/modules/home/pages/details/details_page.dart';
import 'package:empresas/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

class EnterpriseTile extends StatelessWidget {
  Enterprise enterprise;
  EnterpriseTile(this.enterprise);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 150,
            child: CachedNetworkImage(
              imageUrl: enterprise.photo,
              fit: BoxFit.cover,
              height: 200,
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        enterprise.enterpriseName,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20, color: Colors.grey),
                      ),
                      Text(
                        "${enterprise.city} - ${enterprise.country}a",
                        style: TextStyle(
                            fontWeight: FontWeight.w300, fontSize: 14, color: Colors.grey),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Icon(
                            icons.containsKey(enterprise.enterpriseType.enterpriseTypeName)?icons[enterprise.enterpriseType.enterpriseTypeName]:LineAwesomeIcons.circle_o_notch,
                            color: Colors.grey,
                          ),
                          Text(
                            enterprise.enterpriseType.enterpriseTypeName,
                            style: TextStyle(color: Colors.grey),
                          )
                        ],
                      )
                    ],
                  ),
                ),
                RaisedButton(
                  onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailsPage(enterprise))),
                  elevation: 0,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    "Detalhes",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
