import 'package:empresas/app/modules/home/pages/details/details_page.dart';

import 'package:empresas/app/app_module.dart';
import 'package:empresas/app/modules/home/respository/content_repository.dart';
import 'package:empresas/shared/custom_dio/custom.dio.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_controller.dart';
import 'home_page.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController()),
        Bind((i) => ContentRepository(i.get<CustomDio>())),
        Bind((i) => AppModule.to.get<CustomDio>())
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => HomePage()),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
