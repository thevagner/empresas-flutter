import 'package:dio/dio.dart';
import 'package:empresas/app/modules/home/models/enterprise.model.dart';
import 'package:empresas/app/modules/login/models/investor.model.dart';
import 'package:empresas/shared/general_models/error_server.model.dart';
import 'package:empresas/shared/custom_dio/custom.dio.dart';
import 'package:empresas/shared/local_base/investor.helper.dart';
import 'package:empresas/utils/constants.dart';
import 'package:flutter/material.dart';

class ContentRepository{

  CustomDio _customDio;
  ContentRepository(this._customDio);

  dynamic fetchEnterprises({@required String query, int type})async{
    debugPrint("[DB SERVIDOR] ENVIANDO DADOS $query PARA O SERVIDOR");
    try {
      var response = await _customDio.client.get(ENTERPRISES_POINT, queryParameters: {
        "name":query,
        "enterprise_types":type
      });
      List<Enterprise> enterprises = (response.data['enterprises'] as List).map((e) => Enterprise.fromJson(e)).toList();
      print(enterprises.length);
      return enterprises;

    } on DioError catch (e) {
      debugPrint("PROBLEMA AO OBTER DADOS DO SERVIDOR, DADOS CARREGADOS DO BANCO LOCAL");
      if (e.response == null) {
          return ErrorServer(success: false, errors: ["Você esta offline"]);
      }else if (e.response.statusCode==500){
        return ErrorServer(errors: ["Ocorreu algum problema"], success: false);
      } else if (e.response.statusCode != 200) {
        return ErrorServer.fromJson(e.response.data);
      }
    }
  }

}