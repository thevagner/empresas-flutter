// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$resultAtom = Atom(name: '_HomeControllerBase.result');

  @override
  String get result {
    _$resultAtom.reportRead();
    return super.result;
  }

  @override
  set result(String value) {
    _$resultAtom.reportWrite(value, super.result, () {
      super.result = value;
    });
  }

  final _$enterpriseListAtom = Atom(name: '_HomeControllerBase.enterpriseList');

  @override
  ObservableList<Enterprise> get enterpriseList {
    _$enterpriseListAtom.reportRead();
    return super.enterpriseList;
  }

  @override
  set enterpriseList(ObservableList<Enterprise> value) {
    _$enterpriseListAtom.reportWrite(value, super.enterpriseList, () {
      super.enterpriseList = value;
    });
  }

  final _$enterpriseTypeListAtom =
      Atom(name: '_HomeControllerBase.enterpriseTypeList');

  @override
  ObservableList<EnterpriseType> get enterpriseTypeList {
    _$enterpriseTypeListAtom.reportRead();
    return super.enterpriseTypeList;
  }

  @override
  set enterpriseTypeList(ObservableList<EnterpriseType> value) {
    _$enterpriseTypeListAtom.reportWrite(value, super.enterpriseTypeList, () {
      super.enterpriseTypeList = value;
    });
  }

  final _$selectedTypeAtom = Atom(name: '_HomeControllerBase.selectedType');

  @override
  EnterpriseType get selectedType {
    _$selectedTypeAtom.reportRead();
    return super.selectedType;
  }

  @override
  set selectedType(EnterpriseType value) {
    _$selectedTypeAtom.reportWrite(value, super.selectedType, () {
      super.selectedType = value;
    });
  }

  final _$loadingAtom = Atom(name: '_HomeControllerBase.loading');

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  final _$searchAsyncAction = AsyncAction('_HomeControllerBase.search');

  @override
  Future search(String query) {
    return _$searchAsyncAction.run(() => super.search(query));
  }

  final _$setEnterpriseTypeAsyncAction =
      AsyncAction('_HomeControllerBase.setEnterpriseType');

  @override
  Future setEnterpriseType(EnterpriseType enterpriseType) {
    return _$setEnterpriseTypeAsyncAction
        .run(() => super.setEnterpriseType(enterpriseType));
  }

  final _$_HomeControllerBaseActionController =
      ActionController(name: '_HomeControllerBase');

  @override
  dynamic addEnterprise(List<Enterprise> enterprises) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.addEnterprise');
    try {
      return super.addEnterprise(enterprises);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addEnterpriseType(EnterpriseType enterpriseType) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.addEnterpriseType');
    try {
      return super.addEnterpriseType(enterpriseType);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setLoading(bool value) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.setLoading');
    try {
      return super.setLoading(value);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setResultText(String result) {
    final _$actionInfo = _$_HomeControllerBaseActionController.startAction(
        name: '_HomeControllerBase.setResultText');
    try {
      return super.setResultText(result);
    } finally {
      _$_HomeControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
result: ${result},
enterpriseList: ${enterpriseList},
enterpriseTypeList: ${enterpriseTypeList},
selectedType: ${selectedType},
loading: ${loading}
    ''';
  }
}
