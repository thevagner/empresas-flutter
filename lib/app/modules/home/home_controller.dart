import 'package:empresas/app/app_controller.dart';
import 'package:empresas/app/app_module.dart';
import 'package:empresas/app/modules/home/home_module.dart';
import 'package:empresas/app/modules/home/models/enterprise.model.dart';
import 'package:empresas/app/modules/home/respository/content_repository.dart';
import 'package:empresas/app/modules/login/models/investor.model.dart';
import 'package:empresas/shared/general_models/error_server.model.dart';
import 'package:empresas/shared/local_base/investor.helper.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

part 'home_controller.g.dart';

@Injectable()
class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {

  final AppController _appController = AppModule.to.get<AppController>();
  final UserHelper _userHelper = AppModule.to.get<UserHelper>();
  final ContentRepository contentRepository = HomeModule.to.get<ContentRepository>();

  Investor get investor => _appController.investorLogged;

  @observable
  String result = "Use o campo acima para pesquisar empresas";

  @observable
  ObservableList<Enterprise> enterpriseList;
  int get getEnterpriseListLength => enterpriseList==null?0:enterpriseList.length;

  @observable
  ObservableList<EnterpriseType> enterpriseTypeList = ObservableList<EnterpriseType>();

  @observable
  EnterpriseType selectedType;

  String query;

  @observable
  bool loading = false;

  _HomeControllerBase(){
    enterpriseTypeList.add(EnterpriseType(id: null, enterpriseTypeName: "Todos"));
    selectedType = enterpriseTypeList[0];
  }

  signOut()async{
    await _userHelper.deleteUser();
    _appController.setUserLogged(null);
    Modular.to.pushReplacementNamed('/login');
  }

  @action
  search(String query)async {
    setLoading(true);
    dynamic result = await contentRepository.fetchEnterprises(query: query, type: selectedType==null?null:selectedType.id);
    if (result.runtimeType == ErrorServer) {
      ErrorServer errorServer = result;
      setResultText(errorServer.errors[0]);
    } else {
      addEnterprise(result);
      enterpriseList.map((e) => addEnterpriseType(e.enterpriseType)).toList();
    }
    setLoading(false);
  }

    @action
    addEnterprise(List<Enterprise> enterprises){
      enterpriseList = ObservableList<Enterprise>();
      enterpriseList.clear();
      enterpriseList.addAll(enterprises);
      if(enterpriseList.length==0){
        setResultText("Nenhum resultado encontrado");
      }else{
        setResultText("${enterpriseList.length} resultados encontrados");
      }
  }

  @action
  addEnterpriseType(EnterpriseType enterpriseType){
    int typesExist = enterpriseTypeList.where((e) => e.id==enterpriseType.id).length;
    if(typesExist==0){
      enterpriseTypeList.add(enterpriseType);
    }
  }

  @action
  setLoading(bool value)=>loading=value;

  setQUery(String query){
    this.query=query;
    search(query);

    if(query.isNotEmpty){
      search(query);
    }else{
      enterpriseList=null;
      setResultText("Use o campo acima para pesquisar empresas");
    }
  }

  @action
  setResultText(String result)=>this.result=result;

  @action
  setEnterpriseType(EnterpriseType enterpriseType)async{
    selectedType=enterpriseType;
    await search(query);
  }

}