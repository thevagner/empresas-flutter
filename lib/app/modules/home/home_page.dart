import 'dart:math';

import 'package:empresas/app/modules/home/models/enterprise.model.dart';
import 'package:empresas/app/modules/home/widget/enterprise_tile.dart';
import 'package:empresas/app/modules/home/widget/scalling_header.dart';
import 'package:empresas/utils/constants.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:line_awesome_icons/line_awesome_icons.dart';

import 'home_controller.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  //use 'controller' variable to access controller

  final TextEditingController searchEditingController = TextEditingController();
  final FocusNode _searchFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: ()=>unFocus(),
        child: CustomScrollView(
          slivers: <Widget>[
            ScalingHeader(
              flexibleSpaceHeight: 190,
              actions: [
                Container(
                  width: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(100)),
                    color: Colors.transparent,
                    elevation: 0,
                    onPressed: () => controller.signOut(),
                    child: Center(
                      child: Icon(
                        LineAwesomeIcons.sign_out,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                )
              ],
              flexibleSpace: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bg_home.jpg"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Center(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 15),
                    child: Image.asset(
                      "assets/images/logo_home.png",
                      width: 120,
                    ),
                  ),
                ),
              ),
              overlapContentBackgroundColor: Colors.grey[200],
              overlapContentHeight: 100,
              overlapContent: Column(
                children: [
                  TextFormField(
                    onChanged: (query) => controller.setQUery(query),
                    controller: searchEditingController,
                    focusNode: _searchFocus,
                    autofocus: false,
                    decoration: InputDecoration(
                        hintText: "Pesquise por empresa",
                        prefixIcon: Icon(LineAwesomeIcons.search),
                        border: InputBorder.none),
                  ),
                  Observer(
                    builder:(_) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(child: Container(child: Text("Tipo de empresa:", overflow: TextOverflow.fade,))),
                          DropdownButton(
                            isExpanded: false,
                            underline: Container(),
                            value: controller.selectedType,
                            hint: Text("Tipo", style: TextStyle(color: Theme.of(context).accentColor, fontSize: 13),),
                            dropdownColor: Theme.of(context).accentColor,
                            iconEnabledColor: Theme.of(context).accentColor,
                            onChanged: (value)=>controller.setEnterpriseType(value),
                            onTap: ()=>_searchFocus.unfocus(),
                            items: controller.enterpriseTypeList.map((e) => DropdownMenuItem(
                                value: e,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(icons.containsKey(e.enterpriseTypeName)?icons[e.enterpriseTypeName]:LineAwesomeIcons.circle_o_notch,),
                                    SizedBox(width: 5,),
                                    Text(e.enterpriseTypeName, style: TextStyle(color: controller.selectedType==e?Colors.black:Colors.white, fontSize: 13),),
                                  ],
                                ))).toList(),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            SliverToBoxAdapter(
              child: Observer(
                builder: (_) => controller.loading
                    ? Container(
                      width: 80,
                      height: 80 ,
                      child: FlareActor("assets/anims/load_anim.flr",
                          animation: "loading", color: Theme.of(context).accentColor),
                    )
                    : Column(
                        children: [
                          Observer(
                            builder:(_)=> Container(
                              margin: const EdgeInsets.all(15),
                              child: Text(
                                controller.result,
                                style: TextStyle(color: Colors.grey),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          ListView.builder(
                            shrinkWrap: true,
                            padding: EdgeInsets.all(0),
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (context, item) {
                              Enterprise enterprise = controller.enterpriseList[item];
                              int imageIndex = Random.secure().nextInt(4);
                              enterprise.photo = sortedImages[imageIndex];

                              return EnterpriseTile(enterprise);
                            },
                            itemCount: controller.getEnterpriseListLength,
                          ),
                        ],
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }

  unFocus(){
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }
}
